# This is a Nix flake
# It is written in the Nix expression language
{
  description = "Simple flake for simple libcurl example";

  # nixpkgs is the package repository for the Nix package manager
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";

  outputs = { self, nixpkgs }: let
    # In this simple example we choose to build for "x86_64-linux" only
    pkgs = nixpkgs.legacyPackages.x86_64-linux;

    # Declare our own package as a Nix 'derivation'
    wttr-delft = pkgs.stdenv.mkDerivation rec {
      # Our package name
      name = "wttr-delft";

      # Where the source code lives
      src = builtins.path { path = ../src; name = name; };

      # Dependencies
      buildInputs = [
        pkgs.curl.dev
      ];

      # The source code contains only the C file, so we 'manually' compile
      # Note: If we were using Make/CMake/autoconf, the mkDerivation function
      # could handle those automatically.
      # gcc is available by default in pkgs.stdenv.mkDerivation
      buildPhase = "gcc -lcurl -o wttr-delft ./simple.c";

      # Installing is just copying the executable
      installPhase = "mkdir -p $out/bin; install -t $out/bin wttr-delft";
    };
  in
    # These are the flake outputs, i.e. what we can consume
    {
      packages.x86_64-linux = {
        default = wttr-delft;
      };
    };
}
