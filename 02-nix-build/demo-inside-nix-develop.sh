#!/usr/bin/env bash
# shellcheck disable=SC1010,SC2016
set -Eeuo pipefail

dir="$(realpath --relative-to="${PWD}" "$(dirname "${BASH_SOURCE[0]}")")"

source "${dir}/../bash/nix-container-demo-helper.sh"

h "Now we are inside a nix-develop shell!"
x eval 'echo "${name}"'

n "We started this script to continue the demo; for normal use 'nix develop' will drop you into a shell."

h "And we do have gcc:"
x type gcc

h "Not only that, but Nix does all sorts of clever things such as wrapping the compiler:"
x file "$(command -v gcc)"

h "So that required libraries etc. are available:"
x eval 'echo "${NIX_LDFLAGS}"' \| grep -o --color=never -e \'\[^ \]*curl\[^ \]*\'

h "And we can in fact build:"
x gcc -o "${dir}/wttr-delft" -lcurl "${dir}/../src/simple.c"

h "And run!"
x "${dir}/wttr-delft"
