#!/usr/bin/env bash
# shellcheck disable=SC1010,SC2288
set -Eeuo pipefail

dir="$(realpath --relative-to="${PWD}" "$(dirname "${BASH_SOURCE[0]}")")"

source "${dir}/../bash/nix-container-demo-helper.sh"

h "Making our own wttr-delft package"
, "For some reason, we want to have a simple C package that explicitly gets the weather in Delft."
, "We're going to use 'libcurl' for this, and build the executable using nix."

h "The source code is taken from https://curl.se/libcurl/c/simple.html"
x bat --line-range 28: "${dir}/../src/simple.c"

h "Using a Nix flake"
, "The modern (experimental, but recommended) way to use Nix is to use 'flakes':"
,
, "Flakes are the unit for packaging Nix code in a reproducible and discoverable way."
, "They can have dependencies on other flakes, making it possible to have multi-repository Nix projects."
,
, "A flake is a filesystem tree (typically fetched from a Git repository or a tarball)"
, "that contains a file named flake.nix in the root directory."
,
, "flake.nix specifies some metadata about the flake such as dependencies (called inputs),"
, "as well as its outputs (the Nix values such as packages or NixOS modules provided by the flake)."
p

h "So, what does a flake look like?"
x bat "${dir}/flake.nix"

h "Any inputs are pinned with a lock file"
x bat -l json "${dir}/flake.lock"

h "So, without further ado, let's build our package:"
n "If the package was already built and put into the Nix store before this would normally be a no-op,"
, "but for the purposes of an interactive demo we will rebuild it, so we can see what happens."

x nix build "${dir}" -L

h "By default, we get a ./result symlink in the current working directory that points to the result of our build:"
x readlink -f ./result
x find -L "$(readlink -f ./result)"

h "And we can run our executable"
x "./result/bin/wttr-delft"

h "We can also just run the executable directly with 'nix run'"
x rm -f "./result"
x nix run "${dir}"

h "If we want to do development, we also get that 'for free' with 'nix develop':"
n "We do not have gcc available at the moment!"
f type gcc
x nix develop "${dir}" --command "${dir}/demo-inside-nix-develop.sh"
