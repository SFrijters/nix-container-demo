# -*- sh-shell: bash -*-
# shellcheck shell=bash
if [ -z ${_LIBDEMO_INC+x} ]; then
    _LIBDEMO_INC=true

    __libdemo_prev_output=

    # shellcheck disable=SC2120
    __libdemo_use_colour() {
        if [ "${LIBDEMO_USE_COLOUR:-}" = 1 ]; then
            return 0
        elif [ "${LIBDEMO_USE_COLOUR:-}" = 0 ]; then
            return 1
        elif [ "${CLICOLOR_FORCE:-}" = 1 ]; then
            return 0
        elif [ "${CLICOLOR_FORCE:-}" = 0 ]; then
            return 1
        elif [ "${TERM:-}" = dumb ]; then
            return 1
        elif [ -t "${1:-1}" ]; then
            return 0
        else
            return 1
        fi
    }

    __libdemo_use_unicode() {
        if [ "${LIBDEMO_USE_UNICODE:-}" = 1 ]; then
            return 0
        elif [ "${LIBDEMO_USE_UNICODE:-}" = 0 ]; then
            return 1
        elif [ "${TERM:-}" = dumb ]; then
            return 1
        else
            return 0
        fi
    }

    __libdemo_is_interactive() {
        if [ "${LIBDEMO_INTERACTIVE:-1}" = 1 ]; then
            return 0
        else
            return 1
        fi
    }

    demopause() {
        if __libdemo_is_interactive; then
            if __libdemo_use_unicode; then
                if __libdemo_use_colour; then
                    local -r prefix="\e[1;37m[\xE2\x96\xBC]\e[0m"
                else
                    local -r prefix="[\xE2\x86\x93]"
                fi
            else
                if __libdemo_use_colour; then
                    local -r prefix="\e[1;37m[v]\e[0m"
                else
                    local -r prefix="[v]"
                fi
            fi
            echo -e "${prefix}"
            read -n 1 -s -r -p ""
            echo -e "\e[1A\r\e[K"
            echo -e "\e[1A"
            if [[ "${REPLY}" =~ [sS] ]]; then
                return 1
            fi
        else
            echo
        fi
    }

    p() {
        demopause || true
    }

    demoheader() {
        if [ "${__libdemo_prev_output}" != democmd ]; then
            echo
        fi
        if __libdemo_use_colour; then
            local -r prefix="\e[1;34m[#]\e[0m"
            echo -e "${prefix}\e[1m" "$@" "\e[0m"
        else
            local -r prefix="[#]"
            echo -e "${prefix}" "$@"
        fi
        __libdemo_prev_output=demoheader
    }

    h() {
        demoheader "$@"
    }

    demonote() {
        if [ "${__libdemo_prev_output}" != democmd ]; then
            echo
        fi
        if __libdemo_use_colour; then
            local -r prefix="\e[1;33m[!]\e[0m"
        else
            local -r prefix="[!]"
        fi
        echo -e "${prefix}" "$@"
        __libdemo_prev_output=demonote
    }

    n() {
        demonote "$@"
    }

    demowarning() {
        if [ "${__libdemo_prev_output}" != democmd ]; then
            echo
        fi
        if __libdemo_use_colour; then
            local -r prefix="\e[1;31m[!]\e[0m"
        else
            local -r prefix="[!]"
        fi
        echo -e "${prefix}" "$@"
        __libdemo_prev_output=demowarning
    }

    w() {
        demowarning "$@"
    }

    demoecho() {
        if [ "${__libdemo_prev_output}" != demoecho ]; then
            echo
        fi
        local -r prefix="   "
        echo -e "${prefix}" "$@"
        __libdemo_prev_output=demoecho
    }

    ,() {
        demoecho "$@"
    }

    democmd() {
        if [ "${__libdemo_prev_output}" != democmd ]; then
            echo
        fi
        if __libdemo_use_unicode; then
            if __libdemo_use_colour; then
                local -r prefix="\e[1;35m[\xE2\x96\xBA]\e[0m"
            else
                local -r prefix="[\xE2\x9F\xB6]"
            fi
        else
            if __libdemo_use_colour; then
                local -r prefix="\e[1;35m[>]\e[0m"
            else
                local -r prefix="[>]"
            fi
        fi

        if __libdemo_use_colour; then
            echo -e "${prefix}\e[1m" "$@" "\e[0m"
        else
            echo -e "${prefix}" "$@"
        fi
        if demopause; then
           # shellcheck disable=SC2294
           if eval "$@"; then
               if [ "${__libdemo_expect:-}" = failure ]; then
                   return 1
               fi
           else
               if [ "${__libdemo_expect:-}" = success ]; then
                   return 1
               fi
           fi
           demopause
        fi
        __libdemo_prev_output=democmd
    }

    i() {
        if __libdemo_is_interactive; then
            local -r __libdemo_expect=
            democmd "$@" || return 1
        else
            if [ "${__libdemo_prev_output}" != democmd ]; then
                echo
            fi
            if __libdemo_use_colour; then
                local -r prefix="\e[1;31m[!]\e[0m"
                echo -e "${prefix} Skipped interactive command\e[1m" "$@" "\e[0m"
            else
                local -r prefix="[!]"
                echo -e "${prefix} Skipped interactive command" "$@"
            fi
        fi
    }

    x() {
        if __libdemo_is_interactive; then
            local -r __libdemo_expect=
        else
            local -r __libdemo_expect=success
        fi
        democmd "$@" || return 1
    }

    f() {
        if __libdemo_is_interactive; then
            local -r __libdemo_expect=
        else
            local -r __libdemo_expect=failure
        fi
        democmd "$@" || return 1
    }

    democlear() {
        if __libdemo_is_interactive; then
            clear
        fi
        __libdemo_prev_output=democlear
    }

    c() {
        democlear
    }
fi
