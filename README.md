# nix-container-demo

Scripts to (interactively) demonstrate capabilities of [Nix](https://nixos.org/).

## Requirements

Nix with [flakes enabled](https://github.com/mschwaig/howto-install-nix-with-flake-support) must be available.

## Usage

Run all demos in order using

```console
$ ./demo-full.sh
```

or run one particular demo using

```console
./<xx-demo-name>/demo.sh
```

⚠ The program will pause before and after commands are executed to allow some time to absorb what is happening. `[▼]` will be displayed: press any* key to continue.

⚠ If you press 's' when waiting at a pause after a command is printed but before it is executed, it will be skipped instead.

⚠ The demos build a number of obscure variants of packages that are not provided by https://cache.nixos.org/, so this might take a long time the first time you run them.
 Once the packages are present in the Nix store the scripts run in seconds (in automatic mode).

⚠ The container-related demos use [rootless Podman](https://github.com/containers/podman/blob/main/docs/tutorials/rootless_tutorial.md), which needs some configuration files.
If they don't exist yet they will be created in `~/.config/containers/` and will not removed. The scripts do make a best-effort attempt at removing any containers that are created.
Rootless Podman has some special requirements; most of them are provided by Nix, but e.g. [`/etc/subuid` and `/etc/subgid` configuration](https://github.com/containers/podman/blob/main/docs/tutorials/rootless_tutorial.md#etcsubuid-and-etcsubgid-configuration) must be fixed by an administrator.

## Demos

The various demos will show you how to:

* [01-nix-run](01-nix-run/demo.sh): Run Nix packages as an alternative to using a [Docker container](https://www.docker.com/).
* [02-nix-build](02-nix-build/demo.sh): Use Nix to build packages and get a development environment 'for free'.
* [03-nix-container](03-nix-container/demo.sh): Use Nix to build containers and run them using rootless Podman.
* [04-cmake-and-cross](04-cmake-and-cross/demo.sh): Build packages that use [CMake](https://cmake.org/), and cross-compile them for different architectures and static executables.
* [05-static-container](05-static-container/demo.sh): Build a container with only a static binary, combining 03 and 04.
* [06-windows-wine](06-windows-wine/demo.sh): Build a static Windows binary and run it through [wine](https://www.winehq.org/). Here be dragons.
* [07-vm-integration-test](07-vm-integration-test/demo.sh): Perform integration testing using virtual machines.

## Advanced usage

Available options:

```console
$ ./demo-full.sh -h
./demo-full.sh: run all 'demo.sh' scripts in order
  -h|--help|-?: Show usage
  -a|--auto: run in automatic mode (no pausing)
  -g|--gcroots <create|delete|recreate|print>: (re)create, delete, or print Nix garbage collector roots for nix build/run/develop outputs
  -l|--lint: run linter on shell files
```

⚠ The garbage collector roots are made of the form `/nix/var/nix/gcroots/per-user/${USER}/nix-container-demo-*`, but we use an additional indirection via a directory `gcroots` in this repository.
If you remove that directory the symlinks from `/nix/var` will be broken and the garbage collector will no longer consider them roots. This is the easiest way to clean up after running the demos.

Available environment variables:

* `LIBDEMO_USE_COLOUR`: enable (1, default) or disable (0) the use of colour in libdemo. Colours may still appear in output from other packages.
* `LIBDEMO_USE_UNICODE`: enable (1, default) or disable (0) the use of unicode in libdemo. Unicode may still appear in output from other packages.
* `LIBDEMO_INTERACTIVE`: enable (1, default) or disable (0) interactive mode. In non-interactive (automatic) mode, pauses and `i` commands are skipped.
* `WRAP_NIX_MAKE_GCROOTS`: enable (1) or disable (0, default) creation of Nix garbage collector roots for nix build/run/develop outputs.

## libdemo

An attempt at making live, but prepared, presentations in bash.

Demo files `demo.sh` are bash files that source `libdemo/libdemo.sh`, providing some convenience functions and abbreviated forms for them:

* `h`: Print arguments as a header.
* `n`: Print arguments as a note.
* `,`: Print arguments as regular text.
* `x`: Print the provided arguments and execute them, expecting success in automatic mode and ignoring exit status in interactive mode.
       Pauses after the command is shown and after the command is run.
* `f`: Print the provided arguments and execute them, expecting failure in automatic mode and ignoring exit status in interactive mode.
       Pauses after the command is shown and after the command is run.
* `i`: Print the provided arguments and execute them in interactive mode only, ignoring exit status.
       Pauses after the command is shown and after the command is run.
* `p`: Pause until any key is pressed.
* `c`: Clear terminal.

Because of how the command evaluation is structured, care must be taken with characters that bash might interpret, escaping them where necessary.

## Licensing and third party data

* `libdemo`, `nix-container-demo-helper.sh` and the demonstrations themselves are provided under the [MIT license](LICENSE).
* `src/simple.c` is a slightly modified copy of https://curl.se/libcurl/c/simple.html and subject to its own license.
* `data/weather-static` was generated from `curl wttr.in/Delft`.
