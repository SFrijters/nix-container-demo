#!/usr/bin/env bash
# shellcheck disable=SC1010,SC2288
set -Eeuo pipefail

dir="$(realpath --relative-to="${PWD}" "$(dirname "${BASH_SOURCE[0]}")")"

source "${dir}/../bash/nix-container-demo-helper.sh"

h "Putting wttr-delft into a container as a small static binary"
, "We now combine our knowledge of making OCI containers with our knowledge of making static binaries"
x bat "${dir}/flake.nix"

h "Build the container stream"
x nix build "${dir}" -L

n "The output is now a script:"
x bat -l bash ./result

x nix develop "${dir}" --command "${dir}/demo-inside-nix-develop.sh"

i tiv "${dir}/../data/589b561082250818d81e7490.png"
