#!/usr/bin/env bash
# shellcheck disable=SC2288
set -Eeuo pipefail

dir="$(realpath --relative-to="${PWD}" "$(dirname "${BASH_SOURCE[0]}")")"

source "${dir}/../bash/nix-container-demo-helper.sh"

h "Now we are inside a development shell that has rootless podman again."
, "We repeat what we did earlier:"

h "We start by loading the container stream we just made into the registry:"
, "Just pipe it directly into podman"
x "./result | podman load"

h "We can see the final image is there, and it's tiny!"
x "podman image ls | grep 'wttr-delft\|^REPOSITORY'"

, "Some more information is available:"
x "podman inspect localhost/wttr-delft:nix | bat -l json"

h "And now we can run it:"
x "podman run -it localhost/wttr-delft:nix"

h "We have running containers:"
x "podman ps --all --storage | grep 'wttr-delft\|^CONTAINER ID'"

h "Let's kill them and remove them to clean up after ourselves..."
x "podman ps --all --storage | tail -n +2 | grep wttr-delft | awk '{print \$1}' | xargs podman rm"
x "podman image ls | tail -n +2 | grep wttr-delft | awk '{print \$3}' | xargs podman image rm -f"
