{ pkgs }:
# Development shell for interacting with containers
# Based on https://gist.github.com/adisbladis/187204cb772800489ee3dac4acdd9947
let
  # Provides a script that copies/creates files that are required for rootless podman
  podmanSetupScript = import ./podman-setup-script.nix { inherit pkgs; };
in pkgs.mkShell {
  name = "podman";

  packages = with pkgs; [
    podman          # Manage pods, containers and images
    runc            # Container runtime
    conmon          # Container runtime monitor
    skopeo          # Interact with container registry
    slirp4netns     # User-mode networking for unprivileged namespaces
    fuse-overlayfs  # CoW for images, much faster than default vfs
  ];

  shellHook = ''
    # Install configuration required for rootless podman
    ${podmanSetupScript}
  '';
}
