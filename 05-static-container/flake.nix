{
  description = "Example for making a container image with a static binary";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            (final: prev: {
              wttr-delft = prev.callPackage ./wttr-delft.nix {};
            })
          ];
        };

        # We copy only the static binary to avoid pulling in any unwanted dependencies
        wttr-delft-only-binary = pkgs.runCommand "wttr-delft-only-binary" { nativeBuildInputs = [ pkgs.binutils ]; } ''
          mkdir $out
          cp ${pkgs.pkgsCross.musl64.pkgsStatic.wttr-delft}/bin/wttr-delft $out
          # Shrink the executable even more
          chmod u+w $out/wttr-delft
          strip --strip-all $out/wttr-delft
          chmod u-w $out/wttr-delft
        '';

        # Docker image containing only the wttr-delft static binary
        # Actually, this makes a script that dumps the image to stdout on the fly,
        # in this way we can load the data directly into Podman, without having a copy
        # of the image in the Nix store; for large images this is very convenient!
        wttr-delft-container = pkgs.dockerTools.streamLayeredImage {
          name = "wttr-delft";
          tag = "nix";
          config = {
            Cmd = [ "${wttr-delft-only-binary}/wttr-delft" ];
            Env = [ "SSL_CERT_FILE=${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt" ];
          };
        };
      in
        {
          packages.default = wttr-delft-container;

          devShells.default = import ./podman-devshell.nix { inherit pkgs; };
        }
    );
}
