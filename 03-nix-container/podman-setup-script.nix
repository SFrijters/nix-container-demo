{ pkgs }:
let
  registriesConf = pkgs.writeText "registries.conf" ''
    [registries.search]
    registries = ['docker.io']
    [registries.block]
    registries = []
  '';
  storageConf = pkgs.writeText "storage.conf" ''
    [storage]
    driver = "overlay"
    # rootless_storage_path="$XDG_DATA_HOME/containers/storage"
  '';
in pkgs.writeShellScript "podman-setup" ''
  # Dont overwrite customised configuration
  if ! test -f ~/.config/containers/policy.json; then
    >&2 echo "Installing missing ~/.config/containers/policy.json"
    install -Dm644 ${pkgs.skopeo.src}/default-policy.json ~/.config/containers/policy.json
  fi
  if ! test -f ~/.config/containers/registries.conf; then
    >&2 echo "Installing missing ~/.config/containers/registries.conf"
    install -Dm644 ${registriesConf} ~/.config/containers/registries.conf
  fi
  if ! test -f ~/.config/containers/storage.conf; then
    >&2 echo "Installing missing ~/.config/containers/storage.conf"
    install -Dm644 ${storageConf} ~/.config/containers/storage.conf
  fi
''
