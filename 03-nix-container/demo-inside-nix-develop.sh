#!/usr/bin/env bash
# shellcheck disable=SC2288
set -Eeuo pipefail

dir="$(realpath --relative-to="${PWD}" "$(dirname "${BASH_SOURCE[0]}")")"

source "${dir}/../bash/nix-container-demo-helper.sh"

h "Now we are inside a development shell that has podman available."
, "Podman is an alternative implementaion of Docker that can also handle OCI containers."
, "One of the advantages is that it can run rootless, without a daemon."

n "We have added the 'dockerCompat' package so we can use the 'docker' command instead of 'podman'."
, "In this demo we use it interchangably just to show we can."

h "So we start by loading the container we just made into the registry:"
x docker load -i "${dir}/result-container"

h "We can see it's there:"
x "docker image ls | grep 'wttr-delft\|^REPOSITORY'"

h "And now we can run it:"
x docker run -it localhost/wttr-delft:nix

# NOTE: This may suffer from https://github.com/containers/podman/issues/10927
#   "Error: container create failed (no logs from conmon): conmon bytes "": readObjectStart: expect { or n, but found , error found in #0 byte of ...||..., bigger context ...||..."
# Instead of the desired
#   "Error: crun: executable file `sh` not found in $PATH: No such file or directory: OCI runtime attempted to invoke a command that was not found"
# We expect a failure anyway, so this does not break the demo.
h "The image is somewhat minimal, e.g. there is no interactive shell:"
f podman run --entrypoint sh -it localhost/wttr-delft:nix

h "We have running containers:"
x "podman ps --all --storage | grep 'wttr-delft\|^CONTAINER ID'"

h "Let's kill them and remove them to clean up after ourselves..."
x "podman ps --all --storage | tail -n +2 | grep wttr-delft | awk '{print \$1}' | xargs podman rm"
x "podman image ls | tail -n +2 | grep wttr-delft | awk '{print \$3}' | xargs podman image rm -f"
