#!/usr/bin/env bash
# shellcheck disable=SC1010,SC2288
set -Eeuo pipefail

dir="$(realpath --relative-to="${PWD}" "$(dirname "${BASH_SOURCE[0]}")")"

source "${dir}/../bash/nix-container-demo-helper.sh"

h "Putting wttr-delft into a container"
, "Expanding on the previous nix flake makes building a OCI compliant (Docker) image easy:"
x bat "${dir}/flake.nix"

h "We now choose the 'container' package we defined:"
, "This is a 'layered' image, so we might be able to re-use layers at the Docker level."
x nix build "${dir}#container" -L -o "${dir}/result-container"

x ls -lhH "${dir}/result-container"

if ! grep -q "^${USER}:" /etc/subuid; then
    w "No subuid range defined for user, consider running 'sudo usermod --add-subuids 10000-75535 ${USER}' to allow rootless podman to work"
fi

x nix develop "${dir}#podman" --command "${dir}/demo-inside-nix-develop.sh"
