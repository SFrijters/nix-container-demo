{
  description = "Examples of container interactions";

  # nixpkgs is the package repository for the Nix package manager
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";

  outputs = { self, nixpkgs }: let
    # In this simple example we choose to build for "x86_64-linux" only
    pkgs = nixpkgs.legacyPackages.x86_64-linux;

    # wttr-delft is still the same, just moved to another file for clarity
    wttr-delft = import ./wttr-delft.nix { inherit pkgs; };

    # Docker image containing only wttr-delft
    wttr-delft-container = pkgs.dockerTools.buildLayeredImage {
      name = "wttr-delft";
      tag = "nix";
      # Having the container be created 'now' can be convenient to identify
      # different versions of containers, but it breaks reproducibility.
      # So we let Nix set it to a fixed timestamp 0.
      # created = "now";
      contents = [
        wttr-delft
      ];
      config = {
        Cmd = [
          "${wttr-delft}/bin/wttr-delft"
        ];
        # Needed for curl to work
        Env = [
          "SSL_CERT_FILE=${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt"
        ];
      };
    };

    # Development shell for interacting with containers
    # Based on https://gist.github.com/adisbladis/187204cb772800489ee3dac4acdd9947
    podman-devshell = let
      # Provides a script that copies/creates files that are required for rootless podman
      podmanSetupScript = import ./podman-setup-script.nix { inherit pkgs; };
      # Provides a fake "docker" binary mapping to podman
      dockerCompat = pkgs.runCommandNoCC "docker-podman-compat" {} ''
        mkdir -p $out/bin
        ln -s ${pkgs.podman}/bin/podman $out/bin/docker
      '';

    in pkgs.mkShell {
      name = "podman";

      # Packages available in the development shell
      packages = with pkgs; [
        podman          # Manage pods, containers and images
        runc            # Container runtime
        conmon          # Container runtime monitor
        skopeo          # Interact with container registry
        slirp4netns     # User-mode networking for unprivileged namespaces
        fuse-overlayfs  # CoW for images, much faster than default vfs
        dockerCompat    # Aliases for docker / podman
      ];

      shellHook = ''
        # Install configuration required for rootless podman
        ${podmanSetupScript}
      '';
    };

  in
    # These are the flake outputs, i.e. what we can consume
    {
      packages.x86_64-linux = {
        default = wttr-delft;
        container = wttr-delft-container;
      };

      devShells.x86_64-linux = {
        default = wttr-delft;
        podman = podman-devshell;
      };
    };
}
