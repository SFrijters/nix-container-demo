{ pkgs }:
pkgs.stdenv.mkDerivation rec {
  # Our package name
  name = "wttr-delft";

  # Where the source code lives
  src = builtins.path { path = ../src; name = name; };

  # Dependencies
  buildInputs = [
    pkgs.curl.dev
  ];

  # The source code contains only the C file, so we 'manually' compile
  # Note: If we were using Make/CMake/autoconf, the mkDerivation function
  # could handle those automatically.
  # gcc is available by default in pkgs.stdenv.mkDerivation
  buildPhase = "gcc -lcurl -o wttr-delft ./simple.c";

  # Installing is just copying the executable
  installPhase = "mkdir -p $out/bin; install -t $out/bin wttr-delft";
}
