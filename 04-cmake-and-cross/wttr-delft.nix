# This file looks very similar to what official packages in nixpkgs look like
{ stdenv
, cmake
, pkg-config
, curl
}:
stdenv.mkDerivation rec {
  name = "wttr-delft";
  src = builtins.path { path = ../src; name = name; };

  # Build-time dependencies
  # Since we include CMake, Nix will automatically do all the right things:
  # We don't need a custom 'buildPhase' or 'installPhase' anymore.
  nativeBuildInputs = [ cmake pkg-config ];

  # Dependencies
  buildInputs = [ curl.dev ];

  # Strict separation of
  #   nativeBuildInputs (needed at build time only) and
  #   buildInputs (needed at runtime as well)
  strictDeps = true;
}
