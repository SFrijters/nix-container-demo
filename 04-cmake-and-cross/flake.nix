{
  description = "Examples of cross-compilation";

  inputs = {
    # flake-utils is a utility library to work with flakes (duh)
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
  };

  outputs = { self, nixpkgs, flake-utils }:
    # This creates packages for multiple system types
    flake-utils.lib.eachDefaultSystem (system:
      let
        # We import nixpkgs for each particular 'system', and inject
        # wttr-delft into the package set using an overlay.
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            # This is a very simple overlay, but in general they can be used to manipulate nixpkgs,
            # e.g. to change some piece of software to a different version, or add a patch, or... the sky is the limit.
            (final: prev: {
              wttr-delft = prev.callPackage ./wttr-delft.nix {};
            })
          ];
        };
      in
        {
          # The 'default' package is the package injected into our package set
          packages = {
            default = pkgs.wttr-delft;
            # But the package set also has a pkgsCross set, and we can select our package from one of its contents
            cross-riscv64 = pkgs.pkgsCross.riscv64.wttr-delft;
            static = pkgs.pkgsCross.musl64.pkgsStatic.wttr-delft;
            cross-aarch64-static = pkgs.pkgsCross.aarch64-multiplatform.pkgsStatic.wttr-delft;
          };
        }
    );
}
