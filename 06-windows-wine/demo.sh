#!/usr/bin/env bash
# shellcheck disable=SC1010,SC2288
set -Eeuo pipefail

dir="$(realpath --relative-to="${PWD}" "$(dirname "${BASH_SOURCE[0]}")")"

source "${dir}/../bash/nix-container-demo-helper.sh"

h "Cross-compiling a static binary for Windows"
, "Not all platforms are equally well supported, in particular mingw is only supported at Tier 4"
, "See the appendix at https://github.com/NixOS/rfcs/blob/master/rfcs/0046-platform-support-tiers.md"
n "In short: here be dragons!"

i tiv "${dir}/../data/sideeyingchloe.png"

h "We use overrides to force and fix static builds for required libraries"
, "We also strip away some functionality we don't need to make life a bit easier,"
, "and make some small modifications to our own package in the same way:"
x bat "${dir}/flake.nix"

h "Build the package in 64bit mode"
x nix build "${dir}"

h "This is a Windows executable!"
x file ./result/bin/wttr-delft.exe

h The pièce de résistance!
x "WINEPREFIX=$(readlink -f "${dir}/wttr-delft") nix run ${NIXPKGS_FLAKEREF}#wine64 -- ./result/bin/wttr-delft.exe"
