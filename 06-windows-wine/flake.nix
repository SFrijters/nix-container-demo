{
  description = "Example for cross-compiling a Windows binary executable";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            (final: prev: {
              # Disable some CURL features
              curl-minimal = (prev.curl.overrideAttrs (finalAttrs: prevAttrs: {
                # Force static build
                configureFlags = (prevAttrs.configureFlags or [ ]) ++ [ "--enable-static" "--disable-shared" ];
              })).override {
                brotliSupport = false;
                # krb5 fails to build with mingw
                gssSupport = false;
                zstdSupport = false;
                scpSupport = false;
                pslSupport = false;

                # Force static build
                nghttp2 = ((prev.nghttp2.overrideAttrs (finalAttrs: prevAttrs: {
                  configureFlags = (prevAttrs.configureFlags or [ ]) ++ [ "--enable-static" "--disable-shared" ];
                  postPatch = ''
                    sed -i '1i #define NGHTTP2_STATICLIB' lib/includes/nghttp2/nghttp2.h
                  '';
                })).override {
                  # tzdata fails to build with mingw: undefined reference to setenv
                  enableTests = false;
                });

                # Force static build
                libidn2 = prev.libidn2.overrideAttrs (finalAttrs: prevAttrs: {
                  configureFlags = (prevAttrs.configureFlags or [ ]) ++ [ "--enable-static" "--disable-shared" ];
                });
                zlib = prev.zlib.override { static = true; shared = false; };
              };

              wttr-delft = (prev.callPackage ./wttr-delft.nix { curl = final.curl-minimal; }).overrideAttrs (finalAttrs: prevAttrs: {
                # Force static build
                cmakeFlags = [
                  "-DCMAKE_VERBOSE_MAKEFILE:BOOL=ON"
                  "-DCMAKE_EXE_LINKER_FLAGS=-static"
                ];

                # This is just so we don't get colour output, it looks bad when running through wine
                postPatch = ''
                  sed -i 's|https://gitlab.com/SFrijters/nix-container-demo/-/raw/master/data/weather-static|https://wttr.in/Delft?T|' simple.c
                  sed -i '44i curl_easy_setopt(curl, CURLOPT_USERAGENT, "curl");' simple.c
                '';
              });
            })
          ];
        };
      in
      {
        packages.default = pkgs.pkgsCross.mingwW64.wttr-delft;
        devShells.default = import ./podman-devshell.nix { inherit pkgs; };
        formatter = pkgs.nixpkgs-fmt;
      }
    );
}
