# This file looks very similar to what official packages in nixpkgs look like
{ stdenv
, buildPackages
, overrideCC
, cmake
, pkg-config
, curl
, libpsl
}:
let
  # https://discourse.nixos.org/t/statically-linked-mingw-binaries/38395
  # on windows we use win32 threads to get a fully static binary
  gcc = buildPackages.wrapCC (buildPackages.gcc-unwrapped.override ({
    threadsCross = {
      model = "win32";
      package = null;
    };
  }));

  stdenv' = if (stdenv.cc.isGNU && stdenv.targetPlatform.isWindows) then
    overrideCC stdenv gcc
  else stdenv;
in
stdenv'.mkDerivation rec {
  name = "wttr-delft";
  src = builtins.path { path = ../src; name = name; };

  # Build-time dependencies
  # Since we include CMake, Nix will automatically do all the right things:
  # We don't need a custom 'buildPhase' or 'installPhase' anymore.
  nativeBuildInputs = [ cmake pkg-config ];

  # Dependencies
  buildInputs = [ curl.dev libpsl.dev ];

  # Strict separation of
  #   nativeBuildInputs (needed at build time only) and
  #   buildInputs (needed at runtime as well)
  strictDeps = true;
}
