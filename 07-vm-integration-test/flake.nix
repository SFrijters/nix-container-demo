{
  description = "Example for running a VM network test";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";

  outputs = { self, nixpkgs }:
    let
      # NixOS tests only support Linux for now:
      pkgs = import nixpkgs { system = "x86_64-linux"; };

      vm-test = pkgs.nixosTest {
        name = "client-server-test";

        nodes.server = { ... }: {
          networking = {
            firewall = {
              allowedTCPPorts = [ 80 ];
            };
          };
          services.nginx = {
            enable = true;
            virtualHosts."server" = {};
          };
        };

        nodes.client = { pkgs, ... }: {
          environment.systemPackages = [
            pkgs.curl
          ];
        };

        testScript = ''
          # Note: this is Python - the nixosTest wrapper adds some boilerplate
          import time

          # Start client and server at the same time
          start_all()

          # Wait until both machines are ready
          server.wait_for_unit("default.target")
          client.wait_for_unit("default.target")

          # CI runs in a VM without hardware acceleration, so it can be very slow:
          # give it a chance to stabilize before poking the server to increase reliability.
          time.sleep(3)

          # The client makes a http request of the server and should receive the default nginx welcome message
          client.succeed("curl http://server/ | grep -o \"Welcome to nginx!\"")
        '';
      };
    in
      {
        checks.x86_64-linux.default = vm-test;
        packages.x86_64-linux.default = vm-test;
      };
}
