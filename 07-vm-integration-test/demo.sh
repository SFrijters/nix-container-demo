#!/usr/bin/env bash
# shellcheck disable=SC1010,SC2288
set -Eeuo pipefail

dir="$(realpath --relative-to="${PWD}" "$(dirname "${BASH_SOURCE[0]}")")"

source "${dir}/../bash/nix-container-demo-helper.sh"

h "Integration testing using virtual machines"

, "This demo is based on https://nix.dev/tutorials/integration-testing-using-virtual-machines,"
, "but we adapt it to use flakes instead."

, "One of the most powerful features in the Nix ecosystem is the ability to provide"
, "a set of declarative NixOS configurations and use a simple Python interface"
, "to interact with them using QEMU as the backend."

h "Testing an nginx web server"

, "We are going to set up:"
, " * A VM named 'server', running a default nginx installation."
, " * A VM named 'client', running HTTP client queries using curl."
, " * A testScript orchestrating testing logic between client and server."

x bat "${dir}/flake.nix"

# https://github.com/NixOS/nixpkgs/issues/124371
if [ "$(stat -c '%a' /dev/kvm)" != 666 ]; then
    w "If /dev/kvm is not world-read-writable VMs will be emulated which is prohibitively slow!"
fi

h "Let's run it!"
n "A test like this acts just like any other derivation when it is built"
, "So if we've run a successful test before, the result will be in the Nix store"
, "and we will not re-run it automatically."

x nix build "${dir}" -L

h "Flake check"
, "'Checking' a flake involves making sure that all its output packages etc. build,"
, "as well as all explicit checks."

x nix flake check "${dir}" -L
