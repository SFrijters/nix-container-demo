#!/usr/bin/env bash
# shellcheck disable=SC2288
set -Eeuo pipefail

dir="$(realpath --relative-to="${PWD}" "$(dirname "${BASH_SOURCE[0]}")")"

source "${dir}/../bash/nix-container-demo-helper.sh"

h "Getting the weather"
, "With Docker we might use 'docker run curlimages/curl wttr.in/Delft';"
, "with Nix we can use the following command:"
i "nix run nixpkgs#curl -- wttr.in/Delft"

n "'nixpkgs' is a shorthand way to refer to the main Nix package repository."

h "But we may already have had curl on our path?"
x "type curl"

h "So to prove 'nix run' actually works, choose a package we don't already have:"
f "type cowsay"
x "nix run ${NIXPKGS_FLAKEREF}#cowsay -- -f tux hello"

n "In the curl example we were playing fast and loose: the nix run command would just take the latest version."
n "But for cowsay, we pinned it to a particular revision of nixpkgs."
